import 'package:flutter/material.dart';
import 'package:self_diagnose/pages/user_data_extractor.dart';
import 'package:self_diagnose/pages/user_hosp_data_extractor.dart';

class UserProfile extends StatelessWidget {
  UserProfile(this.isHosped, {Key? key}) : super(key: key);

  final bool isHosped;
  final _myScaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _myScaffoldKey,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text('Profile'),
      ),
      body: ListView(
        children: [
          Center(
            child: Column(
              children: <Widget>[
                const SizedBox(
                  height: 10,
                ),
                // UserCard(),
                isHosped ? UserHospCard() : UserCard()
              ],
            ),
          )
        ],
      ),
    );
  }
}
