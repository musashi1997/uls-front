import 'dart:async';

import 'package:flutter/material.dart';
import 'package:self_diagnose/configs/constants.dart';
import 'package:self_diagnose/configs/data_manager.dart';
import 'package:self_diagnose/models/user_login_data.dart';

class UserCard extends StatefulWidget {
  const UserCard({Key? key}) : super(key: key);

  @override
  _UserCard createState() => _UserCard();
}

class _UserCard extends State<UserCard> {
  final Future<UserData> _myData = API.getUser();
  @override
  Widget build(BuildContext context) {
    return Center(
      child: FutureBuilder<UserData>(
        future: _myData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: SizedBox(
                child: FractionallySizedBox(
                  widthFactor: 0.92,
                  //heightFactor: 0.4,
                  child: Container(
                    alignment: Alignment.topLeft,
                    padding: const EdgeInsets.all(20),
                    child: Row(
                      children: [
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        const Align(
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.done,
                            color: Constants.blueDark,
                            size: 24.0,
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '  logged in as: ${snapshot.data?.fname}',
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const CircularProgressIndicator();
        },
      ),
    );
  }
}
