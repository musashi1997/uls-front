import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:self_diagnose/component/SnackbarComponent.dart';
import 'package:self_diagnose/configs/constants.dart';
import 'package:self_diagnose/configs/data_manager.dart';
import 'package:self_diagnose/models/user_hospitalized_data.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:self_diagnose/configs/endpoints.dart';

class UserHospCard extends StatefulWidget {
  const UserHospCard({Key? key}) : super(key: key);

  @override
  _UserHospCard createState() => _UserHospCard();
}

class _UserHospCard extends State<UserHospCard> {
  final Future<UserHospData> _myData = API.getHospUser();
  @override
  Widget build(BuildContext context) {
    return Center(
      child: FutureBuilder<UserHospData>(
        future: _myData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: SizedBox(
                child: FractionallySizedBox(
                  widthFactor: 0.92,
                  //heightFactor: 0.4,
                  child: Container(
                    alignment: Alignment.topLeft,
                    padding: const EdgeInsets.all(20),
                    child: Row(
                      children: [
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        const Align(
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.done,
                            color: Constants.blueDark,
                            size: 24.0,
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '  sending an ambulance. your hospital: ${snapshot.data?.hospitalname}\n roomtype: ${snapshot.data?.roomtype}',
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                          ),
                        ),
                        Container(
                          height: 50,
                          width: 400,
                          margin: const EdgeInsets.only(top: 0),
                          decoration: BoxDecoration(
                            color: Constants.blueDark,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: TextButton(
                            onPressed: () {
                              billing(snapshot.data?.queryid);
                            },
                            child: const Text(
                              'bill',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 25),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const CircularProgressIndicator();
        },
      ),
    );
  }

  showMessage(String message, SnackBarStatus status) {
    snackBarMessage(context, message, status);
  }

  Future billing(int? queryid) async {
    // url to registration php script
    //json maping user entered details
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // final int? queryid = prefs.getInt('queryid');
    final Map mapeddate = {'queryId': queryid.toString()};
    try {
      final response = await http.post(
        Uri.parse(UsersEndpoints.billing),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        encoding: Encoding.getByName('utf-8'),
        body: (mapeddate),
      );

      if (response.statusCode == 200) {
        showMessage(response.body, SnackBarStatus.success);
      } else {
        showMessage(response.body, SnackBarStatus.failure);
      }
    } catch (ex) {
      log(ex.toString());
    }
  }
}
