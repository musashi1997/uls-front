import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:self_diagnose/configs/constants.dart';
import 'package:self_diagnose/configs/endpoints.dart';
import 'package:self_diagnose/pages/result_page.dart';
import 'package:self_diagnose/models/user_hospitalized_data.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'package:self_diagnose/component/SnackbarComponent.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final TextEditingController _fname = TextEditingController();
  final TextEditingController _lname = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _pnumber = TextEditingController();
  final TextEditingController _familyNumber = TextEditingController();
  final TextEditingController _medicalDocs = TextEditingController();
  final TextEditingController _location = TextEditingController();
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final String _response = '';
  String? _status;
  String? _preferredContact;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formkey,
          child: Column(
            children: <Widget>[
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: const EdgeInsets.only(
                  left: 15.0,
                  right: 15.0,
                  top: 15,
                  // bottom: 15,
                ),
                child: TextFormField(
                  controller: _fname,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.person),
                    border: OutlineInputBorder(),
                    labelText: 'first name',
                    hintText: 'Enter first name ',
                  ),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Please enter name';
                    }
                    return null;
                  },
                  onSaved: (fname) {},
                ),
              ),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: const EdgeInsets.only(
                  left: 15.0,
                  right: 15.0,
                  top: 15,
                  bottom: 15,
                ),
                child: TextFormField(
                  controller: _lname,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.person_add),
                    border: OutlineInputBorder(),
                    labelText: 'last name',
                    hintText: 'Enter last name ',
                  ),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Please enter last name';
                    }
                    return null;
                  },
                  onSaved: (lname) {},
                ),
              ),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _email,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.email),
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    hintText: 'Enter valid email as abc@gmail.com',
                  ),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Please enter  email';
                    }
                    if (!RegExp('^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]')
                        .hasMatch(value!)) {
                      return 'Please enter valid email';
                    }
                    return null;
                  },
                  onSaved: (email) {},
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 15.0,
                  right: 15.0,
                  top: 15,
                ),
                //padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _pnumber,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.phone),
                    border: OutlineInputBorder(),
                    labelText: 'Phone number',
                    hintText: 'Enter Phonenumber',
                  ),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Please enter Phonenumber';
                    }
                    return null;
                  },
                  onSaved: (pnumber) {},
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 15.0,
                  right: 15.0,
                  top: 15,
                ),
                //padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _familyNumber,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.phone_in_talk),
                    border: OutlineInputBorder(),
                    labelText: 'Family phone number',
                    hintText: 'Enter Phonenumber',
                  ),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Please enter family phone number';
                    }
                    // if (_password.text != _confirmpassword.text) {
                    //   return "Password Do not match";
                    // }
                    return null;
                  },
                  onSaved: (familyNumber) {},
                ),
              ),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: const EdgeInsets.only(
                  left: 15.0,
                  right: 15.0,
                  top: 15,
                  bottom: 0,
                ),
                child: TextFormField(
                  controller: _location,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.location_city),
                    border: OutlineInputBorder(),
                    labelText: 'Location city',
                    hintText: 'Enter your location city',
                  ),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Please enter your location';
                    }
                    return null;
                  },
                  onSaved: (location) {},
                ),
              ),
              Padding(
                  //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                  padding: const EdgeInsets.only(
                    left: 54.0,
                    right: 15.0,
                    top: 15,
                    bottom: 0,
                  ),
                  child: DropdownButtonFormField(
                    items: const [
                      DropdownMenuItem(
                        value: 'salem',
                        child: Text('  Healthy'),
                      ),
                      DropdownMenuItem(
                        value: 'nimemariz',
                        child: Text('  Not bad'),
                      ),
                      DropdownMenuItem(
                        value: 'mariz',
                        child: Text('  Bad'),
                      )
                    ],
                    onChanged: (String? selectedValue) {
                      setState(() {
                        _status = selectedValue!;
                      });
                    },
                    validator: (value) {
                      if (value == null) {
                        return 'Please enter health status';
                      }
                      return null;
                    },
                    value: _status,
                    isExpanded: true,
                    hint: const Text('  Select your health state'),
                    dropdownColor: Colors.white,
                  )),

              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: const EdgeInsets.only(
                  left: 54.0,
                  right: 15.0,
                  top: 15,
                  bottom: 0,
                ),
                child: DropdownButtonFormField(
                  items: const [
                    DropdownMenuItem(
                      value: 'email',
                      child: Text('  Email'),
                    ),
                    DropdownMenuItem(
                      value: 'sms',
                      child: Text('  SMS'),
                    )
                  ],
                  onChanged: (String? selectedValue) {
                    setState(() {
                      _preferredContact = selectedValue!;
                    });
                  },

                  value: _preferredContact,
                  // icon: const Icon(Icons.flutter_dash),
                  isExpanded: true,
                  validator: (value) {
                    if (value == null) {
                      return 'Please enter preferred way to contact you';
                    }
                    return null;
                  },
                  hint: const Text('  Select preferred contact '),
                  dropdownColor: Colors.white,
                ),
              ),
              Text(
                _response,
                style: TextStyle(color: Colors.red[800]),
              ),
              Container(
                height: 50,
                width: 400,
                margin: const EdgeInsets.only(top: 0),
                decoration: BoxDecoration(
                  color: Constants.blueDark,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: TextButton(
                  onPressed: () {
                    if (_formkey.currentState!.validate()) {
                      registrationUser();
                    }
                  },
                  child: const Text(
                    'Sign Up',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              // Text('New User? Create Account')
            ],
          ),
        ),
      ),
    );
  }
  
  navigateToUserProfile() {
    Navigator.push(context,
      MaterialPageRoute(
        builder: (_) => UserProfile(_status == 'mariz' ? true : false),
      ),
    );
  }

  showMessage(String message, SnackBarStatus status) {
    snackBarMessage(context, message, status);
  }

  Future registrationUser() async {
    // url to registration php script
    //json maping user entered details
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final Map mapeddate = {
      'fname': _fname.text,
      'lname': _lname.text,
      'status': _status,
      'email': _email.text,
      'phone': _pnumber.text,
      'familyPhone': _familyNumber.text,
      'medicalDocs': _medicalDocs.text,
      'preferredContact': _preferredContact,
      'location': _location.text,
    };
    try {
      final response = await http.post(
        Uri.parse(UsersEndpoints.signUp),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        encoding: Encoding.getByName('utf-8'),
        body: (mapeddate),
      );
      if (response.statusCode != 200) {
        showMessage(response.body, SnackBarStatus.failure);
      } else {
        await prefs.clear();
        if (_status == 'mariz') {
          final Map<String, dynamic> decoded =
              json.decode(response.body) as Map<String, dynamic>;
          final UserHospData userHospData = UserHospData.fromJson(decoded);
          prefs.setInt('queryid', userHospData.queryid);
          prefs.setInt('userid', userHospData.userid);
          prefs.setInt('hospitalid', userHospData.hospitalid);
          prefs.setString('hospitalname', userHospData.hospitalname);
          prefs.setString('roomtype', userHospData.roomtype);
        }
        prefs.setString('fname', _fname.text);
        prefs.setString('lname', _lname.text);
        prefs.setString('status', _status!);
        showMessage(response.body, SnackBarStatus.success);
        navigateToUserProfile();
      }
      // getting response from php code, here
      // var data = jsonDecode(reponse.body);

    } catch (ex) {}
  }
}
