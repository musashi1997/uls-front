import 'package:flutter/material.dart';

import 'package:self_diagnose/configs/constants.dart';

class AppThemes {
  static ThemeData light = ThemeData.light().copyWith(
    brightness: Brightness.light,
    hoverColor: Colors.white.withOpacity(0.15),
    scaffoldBackgroundColor: Constants.whiteDark,
    primaryColor: Constants.blueDark,
    canvasColor: Constants.blueDark,
    highlightColor: Constants.blueLight,
    cardColor: Constants.greyLight,
    appBarTheme: const AppBarTheme(
      color: Constants.blueDark,
    ),
  );
}
