
import 'package:flutter/material.dart';
class Constants {
  static const Color redDark = Color(0xffE20C14);
  static const Color redBright = Color(0xffF58286);
  static const Color blueDark = Color(0xff002746);
  static const Color blueLight = Color(0xff6E99A2);
  static const Color limeNormal = Color(0xff87C232);
  static const Color yellowLight = Color(0xffF8F1AE);
  static const Color yellowDark = Color(0xffF8E478);
  static const Color whiteDark = Color(0xffeeeeee);
  static const Color greenLight = Color(0xff16a05d);
  static const Color greyLight = Color(0xffbbbbbbb);
}