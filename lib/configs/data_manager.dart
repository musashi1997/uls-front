import 'dart:async';

import 'package:self_diagnose/models/user_hospitalized_data.dart';
import 'package:self_diagnose/models/user_login_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

class API {
  static Future<UserData> getUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final UserData user = UserData(
      fname: prefs.getString('fname')!,
      lname: prefs.getString('lname')!,
      status: prefs.getString('status')!,
    );
    return user;
  }

  static Future<UserHospData> getHospUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final UserHospData user = UserHospData(
        hospitalid: prefs.getInt('hospitalid')!,
        queryid: prefs.getInt('queryid')!,
        userid: prefs.getInt('userid')!,
        hospitalname: prefs.getString('hospitalname')!,
        roomtype: prefs.getString('roomtype')!);
    return user;
  }

  static Future logOut() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userId');
    prefs.remove('username');
    prefs.remove('token');
    prefs.remove('email');
  }
}
