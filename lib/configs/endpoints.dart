const String baseURL = 'http://localhost:4006/base';

class UsersEndpoints {
  static String signUp = '$baseURL/signForm';
  static String billing = '$baseURL/billlingAPI';
  static String users = '$baseURL/Users';
  // static String register = '$users/register';
}
