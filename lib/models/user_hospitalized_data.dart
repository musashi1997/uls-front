import 'package:json_annotation/json_annotation.dart';

part 'user_hospitalized_data.g.dart';

@JsonSerializable()
class UserHospData {
  final int queryid;
  final int userid;
  final int hospitalid;
  final String hospitalname;
  final String roomtype;

  UserHospData(
      {required this.queryid,
      required this.userid,
      required this.hospitalid,
      required this.hospitalname,
      required this.roomtype});

  factory UserHospData.fromJson(Map<String, dynamic> json) => _$UserHospDataFromJson(json);

  Map<String, dynamic> toJson() => _$UserHospDataToJson(this);
}
