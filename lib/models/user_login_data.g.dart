// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_login_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserData _$UserDataFromJson(Map<String, dynamic> json) => UserData(
      lname: json['lname'] as String,
      fname: json['fname'] as String,
      status: json['status'] as String,
    );

Map<String, dynamic> _$UserDataToJson(UserData instance) => <String, dynamic>{
      'lname': instance.lname,
      'fname': instance.fname,
      'status': instance.status,
    };
