// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_hospitalized_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserHospData _$UserHospDataFromJson(Map<String, dynamic> json) => UserHospData(
      queryid: json['queryid'] as int,
      userid: json['userid'] as int,
      hospitalid: json['hospitalid'] as int,
      hospitalname: json['hospitalname'] as String,
      roomtype: json['roomtype'] as String,
    );

Map<String, dynamic> _$UserHospDataToJson(UserHospData instance) =>
    <String, dynamic>{
      'queryid': instance.queryid,
      'userid': instance.userid,
      'hospitalid': instance.hospitalid,
      'hospitalname': instance.hospitalname,
      'roomtype': instance.roomtype,
    };
