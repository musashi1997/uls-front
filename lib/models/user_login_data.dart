import 'package:json_annotation/json_annotation.dart';

part 'user_login_data.g.dart';

@JsonSerializable()
class UserData {
  final String lname;
  final String fname;
  final String status;

  UserData({
    required this.lname,
    required this.fname,
    required this.status,
  });

  factory UserData.fromJson(Map<String, dynamic> json) => _$UserDataFromJson(json);

  Map<String, dynamic> toJson() => _$UserDataToJson(this);
}
