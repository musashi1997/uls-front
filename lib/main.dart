import 'package:flutter/material.dart';

import 'package:self_diagnose/pages/main_page.dart';
import 'package:self_diagnose/configs/themes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    MaterialApp(
      theme: AppThemes.light,
      themeMode: ThemeMode.light,
      home: const MainPage(),
    ),
  );
}
