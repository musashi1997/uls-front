import 'package:flutter/material.dart';

enum SnackBarStatus {
  success,
  failure
}

snackBarMessage(BuildContext context, String message, SnackBarStatus status) {
  Color statusColor = (status == SnackBarStatus.success) ? Colors.green : Colors.red;
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      duration: const Duration(seconds: 2),
      backgroundColor: Colors.grey[900],
      content: Text(
        message,
        style: TextStyle(color: statusColor),
      ),
    ),
  );
}